import { combineReducers } from 'redux';
import rotateReducer from './rotateReducer';


export default combineReducers({
  rotate: rotateReducer
});
