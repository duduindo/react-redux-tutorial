import React, { Component } from 'react';
import { connect } from 'react-redux';
import rotateAction from 'actions/rotateAction';
import logo from './logo.svg';
import './App.css';

const mapStateToProps = state => {
  return { ...state };
};

const mapDispatchToProps = dispatch => ({
  rotateAction: payload => dispatch(rotateAction(payload)),
});


class App extends Component {
  render() {
    console.log(this.context);

    return (
      <div className="App">
        <header className="App-header">
          <img
            src={logo}
            className={
              `App-logo ${ (this.props.rotate.rotating ? '' : 'App-logo-paused') }`
            }
            alt="logo"
            onClick={ () => this.props.rotateAction(!this.props.rotate.rotating) }
          />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
