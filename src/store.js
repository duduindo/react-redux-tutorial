import { createStore, combineReducers } from 'redux';
import reducer from './reducers';
import rotateReducer from './reducers/rotateReducer';

function configureStore() {
  return createStore( reducer );
}

export default configureStore;
